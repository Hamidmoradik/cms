FROM node:latest
# Create application directory
RUN mkdir -p /usr/src/app
# Change current working directory to app directory
WORKDIR /usr/src/app
# Copy pakage.json to workdir and installing dependencies
COPY package.json /usr/src/app/
# Install all dependencies
RUN npm i
# Copy the code to workdir
COPY . /usr/src/app/

EXPOSE 2099

CMD ["npm", "run", "DEBUG"]